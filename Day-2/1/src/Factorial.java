import java.util.Scanner;

public class Factorial {

	public static int factOfN(int n){
		int fact =1;
		while(n>1){
			fact = fact * n;
			n--;
		}
		return fact;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter n value : ");
		int n = sc.nextInt();
		sc.close();
		System.out.println("Factorial of integer "+n+"  is "+factOfN(n));

	}

	
}
