import java.util.Scanner;

public class Temperature {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
        System.out.println("Temperature Converter Menu:");
        System.out.println("1. Temperature in Celsius ");
        System.out.println("2. Temperature in Fahrenheit ");
        System.out.print("Enter your choice (1 or 2) : ");
        
        int choice = sc.nextInt();
        if (choice == 1) {
        	System.out.println("Celsius to Fahrenheit");
        	System.out.print("Enter Tmperature in Celsius : ");
            double celsius =  sc.nextDouble();
            double fahrenheit = (celsius * 9/5) + 32;
            System.out.println("Temperature in Fahrenheit: " + fahrenheit);
        } else if (choice == 2) {
            System.out.println("Fahrenheit to Celsius");
            System.out.print("Enter Temperature in Fahrenheit : ");
            double fahrenheit =  sc.nextDouble();
            double celsius = (fahrenheit - 32) * 5/9;
            System.out.println("Temperature in Celsius: " + celsius);
        } else {
            System.out.println("Invalid choice. Please enter 1 or 2.");
        }
        sc.close();

	}

}
