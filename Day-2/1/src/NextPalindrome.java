
import java.util.Scanner;

public class NextPalindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number of Test Cases : ");
        int t = sc.nextInt();

        for (int i = 0; i < t; i++) {
        	System.out.print("Enter N value : ");
            int n = sc.nextInt();
            int nextPalindrome = findNextPalindrome(n);
            System.out.println(nextPalindrome);
        }

        sc.close();
    }

    private static int findNextPalindrome(int n) {
        while (true) {
            n++;
            if (isPalindrome(n)) {
                return n;
            }
        }
    }

    private static boolean isPalindrome(int num) {
        int originalNum = num;
        int reversedNum = 0;

        while (num > 0) {
            int digit = num % 10;
            reversedNum = reversedNum * 10 + digit;
            num /= 10;
        }

        return originalNum == reversedNum;
    }
}
