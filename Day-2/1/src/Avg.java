import java.util.Scanner;

public class Avg {

	public static int Average(int arr[], int n){
		int sum =0;
		for(int i=0; i<n; i++){
			sum = sum + arr[i];
			
		}
		int r = sum/n;
		return r;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter n value : ");
		int n = sc.nextInt();
		int arr[]= new int[n];
		for (int i =0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		sc.close();
		System.out.println("Average of "+ n + " numbers :" + Average(arr,n));
	}

	
}
