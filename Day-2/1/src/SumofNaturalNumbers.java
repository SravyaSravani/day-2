import java.util.*;

public class SumofNaturalNumbers {

	public static int sumOfN(int n){
		int sum =0;
		while(n>0){
			sum = sum + n;
			n--;
		}
		return sum;
	}
	

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println("sum of n natural numbers is "+sumOfN(n));
		sc.close();
	}

	
}
