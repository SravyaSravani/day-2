import java.util.Scanner;

public class SumofDigits {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a value : ");
		int a = sc.nextInt();
		int b = 0;
		int sum = 0;
		while (a>0){
			b = a%10;
			sum = sum +b;
			a = a/10;
			
		}
		System.out.println("Sum of Digits : " + sum);
		sc.close();

	}

}
