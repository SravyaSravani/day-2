import java.util.Scanner;

public class DuplicateChar { 

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter a string: ");
		String str = scanner.nextLine();

		int[] charCount = new int[256];

		char[] charArray = str.toCharArray();

		for (char c : charArray) {
			charCount[c]++;
		}

		System.out.println("Duplicate characters in the string:");
		for (int i = 0; i < charCount.length; i++) {
			if (charCount[i] > 1) {
				System.out.println((char) i + " - " + charCount[i] + " times");
			}
		}

		scanner.close();
	}
}
