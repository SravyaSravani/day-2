
import java.util.Scanner;

public class SmallestLargest {


	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the number of elements in the array: ");
		int n = input.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter the elements of the array:");
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		int smallest = arr[0];
		int largest = arr[0];
		for (int i = 1; i < n; i++) {
			if (arr[i] < smallest) {
				smallest = arr[i];
			}
			if (arr[i] > largest) {
				largest = arr[i];
			}
		}
		input.close();
		System.out.println("Smallest element in the array : " + smallest);
		System.out.println("Largest element in the array : " + largest);
	}
}

