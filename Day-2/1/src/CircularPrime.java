import java.util.Scanner;

public class CircularPrime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = scanner.nextInt();

        if (isCircularPrime(num)) {
            System.out.println(num + " is a circular prime.");
        } else {
            System.out.println(num + " is not a circular prime.");
        }

        scanner.close();
    }

    private static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static int rotateNumber(int num) {
        String numStr = Integer.toString(num);
        numStr = numStr.substring(1) + numStr.charAt(0);
        return Integer.parseInt(numStr);
    }

    private static boolean isCircularPrime(int num) {
        if (!isPrime(num)) {
            return false;
        }

        int numDigits = (int) Math.log10(num) + 1;

        for (int i = 0; i < numDigits - 1; i++) {
            num = rotateNumber(num);
            if (!isPrime(num)) {
                return false;
            }
        }

        return true;
    }
}
