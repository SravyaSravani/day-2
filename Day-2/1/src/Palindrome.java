
public class Palindrome {

	public static void main(String[] args) {
		
		String s ="Rama";
		String s1 ="Ramula";
		System.out.println(s);
		System.out.println(s1);
		String s2 ="";
		String s3 ="";
		
		for (int i=s.length()-1;i>=0; i--){
			s2 += s.charAt(i); 
		}
		for (int i=s1.length()-1;i>=0; i--){
			s3 += s1.charAt(i);
		}
		
		if(s2==s && s3==s1){
			System.out.println("s and s1 both are Palindrome");
			System.out.println(s2);
			System.out.println(s3);
		}else if(s2==s && s3!=s1){
			System.out.println("s is a Palindrome and s1 is not a Palindrome");
			System.out.println(s2);
			System.out.println(s3);
		}else if(s2!=s && s3==s1){
			System.out.println("s is not a Palindrome and s1 is a Palindrome");
			System.out.println(s2);
			System.out.println(s3);
		}else{
			System.out.println("s and s1 both are not Palindrome");
			System.out.println(s2);
			System.out.println(s3);
		}
	}

	
}
