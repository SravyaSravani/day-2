import java.util.*;

public class BinarySearch {

	public static int binarySearch(int k, int n, int arr[]) {
        int low = 0;
        int high = n - 1;

        while (low <= high) {
            int mid = low + (high - low) / 2;

            if (arr[mid] == k) {
                return mid; 
            } else if (arr[mid] < k) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }

        return -1; 
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter n value : ");
        int n = sc.nextInt();
        System.out.print("Enter k value : ");
        int k = sc.nextInt();
        int arr[] = new int[n];
        
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int result = binarySearch(k, n, arr);
        if (result != -1) {
            System.out.println("Key is at index: " + result);
        } else {
            System.out.println("Key not found");
        }
        
        sc.close();
    }

	
}
